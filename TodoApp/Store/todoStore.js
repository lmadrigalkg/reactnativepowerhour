import { createStore, applyMiddleware } from 'redux';
import todoReducer from './../Reducers/todoReducer'
import thunk from 'redux-thunk';

export default function configureStore(initialState) {
  return createStore(
    todoReducer,
    initialState,
    applyMiddleware(thunk)
  );
}
