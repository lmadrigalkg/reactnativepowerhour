import React, { Component, PropTypes } from 'react';

import { NavigationActions } from 'react-navigation'
var DismissKeyboard = require('dismissKeyboard');

import {
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Keyboard,
  StyleSheet,
} from 'react-native';
import Render from './Render';

export default class TaskForm extends Component {
	constructor(props, context) {
		super(props, context);
    this.state = {
      text: "",
    };
	}
  
	render() {
		return Render.bind(this)(styles);
	}

  goHome() {
    const backAction = NavigationActions.back();
    this.props.navigation.dispatch(backAction);
  }

  onAddPressed() {
    const { params } = this.props.navigation.state;
    if(this.state.text !== "") {
      const id = Math.floor((Math.random() * 100) + 20);
      params.onAddPressed({id: id, task: this.state.text});
      this.goHome();
      DismissKeyboard();
    }
  }

  onCancelPressed() {
    this.goHome();
    DismissKeyboard();
  }
}

const styles = StyleSheet.create({
  mainText: {
    paddingTop: 20,
    paddingBottom: 20,
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
    margin: 'auto',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#aad1ff',
  },
  textInput: {
  	backgroundColor: 'white',
    marginBottom: 20,
    padding: 10,
    margin: 20,
  },
  buttonsContainer: {
  	padding: 10,
  	flexDirection: 'row',
  	alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    fontSize: 18,
    color: 'white',
    fontWeight: '600',
  },
  button: {
    alignSelf: 'stretch',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    height: 45,
    backgroundColor: '#56e2aa',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonCancel: {
    backgroundColor: '#ef7a91',
  },
});