import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Navigator,
} from 'react-native';
import TaskList from './../TaskList';
import TaskForm from './../TaskForm/Component';
import Icon from 'react-native-vector-icons/MaterialIcons';
import configureStore from './../../Store/todoStore';
import * as actions from './../../Actions/index';
import * as types from './../../Actions/actionTypes';

const store = configureStore();
export default class TodoApp extends Component {
  constructor(props, context){
    super(props, context);
    this.onLoadTask();
    store.subscribe(() => {
      this.setState(store.getState());
    });
  }  

  render() {
    if(this.state != null){
      return (
        <View style={styles.container}>
          <ScrollView contentContainerStyle={styles.listContainer}>
            <TaskList todos={this.state.todos} title={"To do"} onTaskCompleted={this.onTaskCompletedDelegate.bind(this)}/>
          </ScrollView>
          <View style={styles.addButton} >
            <Icon.Button name="add-circle" size={60} color="white" backgroundColor="rgba(0,0,0,0)" onPress={this.onAddPressed.bind(this)}></Icon.Button>
          </View>
        </View>
      );
    }
    else return (
      <View/>
    )
  }

  onLoadTask() {
    store.dispatch(actions.getTodos());
  }

  onTaskCompletedDelegate(todo) {
    store.dispatch(actions.deleteTodo(todo.id));
  }

  onAddDelegate(todo) {
    store.dispatch(actions.addTodo(todo));
  }

  onAddPressed() {
    const { navigate } = this.props.navigation;
    navigate('TaskForm', { onAddPressed: this.onAddDelegate.bind(this)});
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: '#aad1ff',
  },
  listContainer:{
    paddingRight: 20,
    paddingLeft: 20,
    justifyContent: 'center',
  },
  addButton: {
    bottom: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
