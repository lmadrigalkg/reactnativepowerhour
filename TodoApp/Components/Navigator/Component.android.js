import React, { Component } from 'react';

import { StackNavigator } from "react-navigation";
import TodoApp from './../TodoApp/Component';
import TaskForm from './../TaskForm/Component';


const stackNavigatorConfiguration = {
    initialRouteName:'Home',
    headerMode: 'screen',
    navigationOptions: {
      title: 'Todo'
    }         
}

export default AppNavigator = StackNavigator(
  {
    Home: { screen: TodoApp },
    TaskForm: { screen: TaskForm }
  }, stackNavigatorConfiguration);

