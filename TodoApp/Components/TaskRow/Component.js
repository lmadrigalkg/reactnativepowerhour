import React, { Component, PropTypes } from 'react';
import {
  StyleSheet
} from 'react-native';
import Render from './Render';

export default class TaskRow extends Component {
	constructor(props, context) {
		super(props, context);
	}
	render() {
		return Render.bind(this)(styles);
	}
  onDonePressed() {
    this.props.onTaskCompleted(this.props.todo);
  }
}

const styles = StyleSheet.create({
  rowContainer: {
    flexDirection: 'row',
    height: 50,
    borderRadius: 5,
    backgroundColor: 'white',
    paddingLeft: 5,
    marginBottom: 5,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  doneText: {
    fontSize: 10,
    fontWeight: '500',
  },
  doneLabel: {
    marginRight: 10,
    backgroundColor: '#ededed',
    padding: 5,
  },
  rowText : {
    fontSize: 16,
    color: 'black',
  },
});