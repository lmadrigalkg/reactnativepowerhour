import * as types from '../Actions/actionTypes';
import * as actions from '../Actions/index';

const defaultState = {
	todos : [	
	],
};

export default function todoReducer(state = defaultState, action) {
	switch(action.type) {
		case types.GET_TODOS:
			return {
				todos: (Object.assign([], action.todos))
			};
		case types.ADD_TODO_SUCCESS:
			return {
				todos: (Object.assign([], action.todos))
			};
		case types.ADD_TODO_FAILURE:
			console.warn('Add Error: ', action.err);

		case types.DELETE_TODO_SUCCESS:
			return {
				todos: (Object.assign([], action.todos))
			};
		default:
			return state;
	}
}
