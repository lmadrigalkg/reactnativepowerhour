
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
} from 'react-native';

import AppNavigator from './Components/Navigator/Component';
import configureStore from './Store/todoStore';
import * as actions from './Actions/index';

const store = configureStore();
store.dispatch(actions.getTodos());

AppRegistry.registerComponent('TodoApp', () => AppNavigator);
