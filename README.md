# README #

React Native + Redux example.

### How do I get set up? ###

* Follow the instructions to install react native in [React Native Getting Started](https://facebook.github.io/react-native/docs/getting-started.html)
* run npm install
* run command react-native run-ios to execute the iOS version or react-native run-android to execute the Android version